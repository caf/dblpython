#! /usr/bin/env python

import sys, os, os.path
prog_files = './dblpython/'
sys.path.insert(0, os.path.abspath(prog_files))
from DBLPParse import DBLP_url, DTD_url
from bibfromhints import DB_FILE, dblp_filename, dtd_filename, db_object


if __name__ == '__main__':
    try:
        import wget
        wget = True
    except ImportError:
        wget = False
    try:
        import pylatexenc
        escape = True
    except ImportError:
        escape = False
    try:
        import pybtex
        text = True
    except ImportError:
        text = False

    print >> sys.stderr, ""
    if not wget and \
       (not os.path.isfile(os.path.join(prog_files, dblp_filename)) or \
        not os.path.isfile(os.path.join(prog_files, dtd_filename))):
        print >> sys.stderr, """ Package 'wget' not installed. DBLPython needs it to download the
 DBLP database in XML format.

 Solutions:
    1. Install the package with: pip install wget.
    2. Manually download into %s the files 
       at the following two URLs:
         %s
         %s""" % (prog_files, DBLP_url, DTD_url)
        sys.exit(1)

    if os.path.isfile(os.path.join(prog_files, DB_FILE)):
        print >> sys.stderr, """ A database file %s already exists in %s. 
 If you want to build it from scratch, delete it and 
 then run this init script again.""" % (DB_FILE, prog_files)
    else:
        print >> sys.stderr, """ Database initialization starting. This will take some time 
 (around 30 minutes) and create a file %s. 
 Talk to you in a while...""" % DB_FILE
        db = db_object(DB_FILE, prog_files, update=False)

    if not escape:
        print >> sys.stderr, ""
        print >> sys.stderr, """ Package 'pylatexenc' not installed. DBLPython needs it to
 properly escape characters in TeX output. Without it 
 DBLPythin will just drop all non-ASCII characters.

 Solution: Install the package with: pip install pylatexenc."""

    if not pybtex:
        print >> sys.stderr, ""
        print >> sys.stderr, """ Package 'pybtex' not installed. DBLPython needs it to
 render bibliographic entries in plain text. This is not needed 
 if you only want to generate BibTeX output; otherwise install 
 the package with: pip install pybtex."""
