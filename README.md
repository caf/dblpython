# DBLPython #
**DBLPython** is a collection of scripts and data structures to query bibliographical data from the [DBLP](http://dblp.uni-trier.de/) database and display it in customized [BibTeX](http://www.bibtex.org/) format.


## Installation ##

**DBLPython** queries DBLP data serialized in an [SQLite](https://www.sqlite.org/) relational database. The database is created upon first usage by downloading and indexing the complete DBLP database in XML format. You can also avoid generating the database and use an in-memory index instead, but this has the major drawback of being non-persistent (and of requiring a lot of main memory). 

This is how you install and initialize **DBLPython**:

1. Download or clone the repository.
2. Run the script `init.py` in the repository's root directory. This checks for unmet dependencies and generates the database. It takes a while (around 30 minutes) and uses up to 6 GB of disk space. After it is done, you can delete the file `dblp.xml.gz` since all the information is now stored in the database file `DBLP.db`.
3. Add the subdirectory `dblpython` (relative to the repository's root) to the path, or create links to the scripts `bibfromhints.py` and `rec2bib.py`.
4. Optionally, run the tests in subdirectory `tests` (in particular, `test_db.py` checks that the database file has been generated correctly).


## Usage ##

**DBLPython** consists of two main scripts:

1. `bibfromhints.py` queries the DBLP database and extracts information in [JSON](http://www.json.org) format.
2. `rec2bib.py` reads JSON input and formats it as BibTeX or plain text.

### Bibliography from hints ###

A _hint_ is partial bibliographic information consisting of a list of author names and a partial title. For example, to get a paper by _Carlo A. Furia_ whose title begins with _To run what_ we give the hint:
```
$ bibfromhints.py -a "carlo a. furia" -t "to run what"
```
This should output one record corresponding to [this RV paper](http://bugcounting.net/publications.html#rv13).
A hint need not have authors but needs at least a partial title; lookup of hints with title only is in general faster than lookup of hints with authors and title, because the latter trigger nested SQL queries.

Multiple hints can be passed to `bibfromhints.py` in a file, one hint per line using the format:
```
tom, dick, harry : foo bar baz
```
which looks up for publications with at least three authors with full names _tom_, _dick_, and _harry_, and whose title begins with _foo bar baz_. Capitalization and non-alphanumeric characters are ignored in author names and titles.

### BibTeX from bibliographic records ###

Given a list of JSON records in the format produced by `bibfromhints.py`, `rec2bib.py` adjusts their content and outputs it in BibTeX (or plain text) format.
For example, to output only authors, title, and year information:
```
$ rec2bib.py -F author title year -- records.json
```
where `records.json` stores output produced by an invocation of `bibfromhints.py`.

### Output ###

Both scripts `bibfromhints.py` and `rec2bib.py` write all status information on standard error, and use standard output only for the actual bibliographic information. Therefore, we can combine them using pipes:
```
$ bibfromhints.py -a "carlo a. furia" -t "to run what" | rec2bib.py -F title author year
```
which prints something like:
```
@inproceedings{conf/rv/PolikarpovaFW13,
title       = {To Run What No One Has Run Before: Executing an Intermediate Verification Language.},
author      = {Nadia Polikarpova and Carlo A. Furia and Scott West},
year        = {2013}
}
```

In general, `bibfromhints.py` extracts as much information as possible from DBLP; in particular, it updates the `booktitle` field in records of papers appeared in conference proceedings by looking up cross-references to other DBLP records. It also tries to guess acronyms of conferences based on the list in `CS_conferences.txt`. If you find a conference not listed there, add it to the file. 

Then, `rec2bib.py` takes care of formatting the information, possibly stripping it down (for example, by removing unnecessary fields), including escaping non-ASCII characters in TeX format, adding braces around likely acronyms that must have a fixed capitalization (according to the list in `proper_names.txt`), and generating unique readable keys.

The main advantage of having two scripts is that you can still manually tweak the JSON file before running it through `rec2bib.py` to introduce any change that is not handled automatically. For example, you can force the capitalization of any name by adding braces around it.

### Updating the database ###

Normally, `bibfromhints.py` tries to create a database by parsing DBLP XML data only if a database file does not already exist; and it tries to download DBLP XML data only if it is creating a database and the data is not already available locally. The option `-u` or `--update` makes `bibfromhints.py` lookup the timestamp of the online DBLP XML data file; if that timestamp is newer than the timestamp of the local copy or the local copy does not exist, it will download the latest version and update the database with the new records. Updating is in general significantly faster than generating the whole database from scratch.

### More options ###

Use the flag `-h` to both scripts `bibfromhints.py` and `rec2bib.py` to learn about other command-line options to control output.


## API ##

You can use **DBLPython** programmatically through its API. See the tests (subdirectory `tests`) for examples, as well as the main body of the scripts `bibfromhints.py` and `rec2bib.py`.


## License ##

**DBLPython** is copyright 2016 [Carlo A. Furia](http://bugcounting.net). It is distributed under GLP v. 3 or later; see the file `COPYING` for a text of the GLP license.
