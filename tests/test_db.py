#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
prog_files = '../dblpython/'
sys.path.insert(0, os.path.abspath(prog_files))

import unittest
import bibfromhints
from bibdata import BibJSON

class Test_lookup(unittest.TestCase):

    def setUp(self):
        self.hints_filename = 'hints'

    def test_hint_creation(self):
        self.hints = bibfromhints.read_hints(self.hints_filename)
        self.assertEqual(len(self.hints), 3)

    def test_db_lookup(self):
        db = bibfromhints.db_object(bibfromhints.DB_FILE, prog_files, update=False)
        self.test_hint_creation()
        bib = bibfromhints.pubs_from_hints(self.hints[:1], db, acronyms=[], clean=True)
        self.assertEqual(len(bib), 2)
        bib = bibfromhints.pubs_from_hints(self.hints, db, acronyms=[], clean=True)
        self.assertEqual(len(bib), 4)


if __name__ == '__main__':
    lookup = unittest.TestLoader().loadTestsFromTestCase(Test_lookup)
    suite = unittest.TestSuite([lookup])
    unittest.TextTestRunner(verbosity=2, buffer=True).run(suite)
