#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, gzip
sys.path.insert(0, os.path.abspath('../dblpython/'))

import unittest
import os.path
from bibdata import *

class Test_bibdata_Authors_Title(unittest.TestCase):

    def setUp(self):
        self.name = 'John R. Doe'
        self.author = Author(self.name)
        self.a1 = Author('John R. Doe')
        self.b1 = Author('john r doe')
        self.a2 = Author('Jane x. Roe')
        self.b2 = Author('Jane X. Roe')
        self.authors_a1a2 = Authors([self.a1, self.a2])
        self.authors_a2a1 = Authors([self.a2, self.a1])
        self.authors_b2b1 = Authors([self.b2, self.b1])
        self.authors_b2 = Authors([self.b2])
        self.t = 'About: a meta-title.'
        self.title = Title(self.t)

    def test_simple_name(self):
        self.assertEqual(simple_name('Joh#n   $D(oe'),
                         simple_name('John Doe'))

    def test_simple_name_also_keep(self):
        self.assertNotEqual(simple_name('Joh#n   $D(oe', also_keep=[' ']),
                            simple_name('John Doe'))

    def test_Author_create(self):
        self.assertEqual(self.author.name, self.name)

    def test_Author_compare(self):
        self.assertTrue(self.a1 == self.b1)

    def test_Authors_compare(self):
        self.assertTrue(self.authors_a1a2 == self.authors_b2b1)
        self.assertNotEqual(unicode(self.authors_a1a2), unicode(self.authors_b2b1))
        self.assertTrue(self.authors_b2 <= self.authors_a1a2)
        self.assertTrue(self.authors_b2 <= self.authors_a2a1)
        self.assertTrue(self.authors_a2a1 >= self.authors_b2)

    def test_Title_create(self):
        self.assertEqual(self.title.title, self.t)

    def test_Title_compare(self):
        mt = 'Aboutametatitle!'
        self.assertTrue(self.title == Title(mt))
        self.assertTrue(Title(mt[:4]) <= self.title)
        self.assertTrue(self.title >= Title(mt[:4]))

    def test_app(self):
        s = 'XXX'
        f = lambda x: s
        self.author.app(f)
        self.assertEqual(self.author, Author(s))
        self.authors_a1a2.app(f)
        self.assertEqual(self.authors_a1a2, Authors([Author(s), Author(s)]))
        self.title.app(f)
        self.assertEqual(self.title, Title(s))


class Test_bibdata_Publication(unittest.TestCase):

    def setUp(self):
        self.name1 = 'John R. Doe'
        self.author1 = Author(self.name1)
        self.name2 = 'Jane X. Roe'
        self.author2 = Author(self.name2)
        self.authors = Authors([self.author1, self.author2])
        self.title = Title('A meta-study in the abstract world')
        self.pubdict = {'author': self.authors,
                        'title': self.title,
                        'booktitle': "Some booktitle",
                        'btype': "inproceedings",
                        'extra_field': "whatever"}
        self.dblp_record = {'type': "type",
                            'key': "key",
                            'title': "title",
                            'author': [self.author1.name, self.author2.name],
                            'booktitle': "booktitle",
                            'editor': self.author2.name,
                            'journal': "journal",
                            'pages': "pages",
                            'year': "year",
                            'month': "month",
                            'series': "series",
                            'volume': "volume",
                            'number': "number",
                            'publisher': "publisher",
                            'ee': "ee",
                            'school': "school",
                            'institution': "institution",
                            'chapter': "chaper",
                            'note': "note",
                            'crossref': "crossref",
                            'acronym': "acronym"}

    def test_empty(self):
        p = Publication()
        for f in p.fieldnames:
            self.assertTrue(p.__dict__[f] == p.fieldnames[f])

    def test_from_dict(self):
        p = Publication(from_dict=self.pubdict)
        for f in p.fieldnames:
            if f in self.pubdict.keys():
                self.assertTrue(p.__dict__[f] == self.pubdict[f])
            else:
                self.assertTrue(p.__dict__[f] == p.fieldnames[f])

    def test_from_DBLP(self):
        p = Publication(from_DBLP=self.dblp_record)
        for f in p.fieldnames:
            if f == 'btype':
                v = self.dblp_record['type']
            elif f == 'author':
                v = self.authors
            elif f == 'editor':
                v = Authors([self.author2])
            else:
                v = self.dblp_record[f]
            self.assertEqual(p.__dict__[f], v)

    def test_from_other_empty(self):
        p = Publication()
        q = Publication(from_other=p)
        for f in p.fieldnames:
            self.assertEqual(p.__dict__[f], q.__dict__[f])

    def test_from_other_dict(self):
        p = Publication(from_dict=self.pubdict)
        q = Publication(from_other=p)
        for f in p.fieldnames:
            self.assertEqual(p.__dict__[f], q.__dict__[f])

    def test_serialization(self):
        p = Publication()
        q = Publication(from_dict=self.pubdict)
        r = Publication(from_DBLP=self.dblp_record)
        for x in [p, q, r]:
            x_prime = BibJSON.loads(BibJSON.dumps(x))
            for f in x.fieldnames.keys() + x_prime.fieldnames.keys():
                self.assertEqual(x.__dict__[f], x_prime.__dict__[f])

                
class Test_DBLPParse(unittest.TestCase):

    import DBLPParse

    def setUp(self):
        self.DBLPParse.dblp_filename = '../tests/miniDB.xml.gz'
        self.DBLPParse.dtd_filename = '../tests/miniDB.dtd'
        self.xml_file = gzip.GzipFile(self.DBLPParse.dblp_filename, 'r')
        self.DBLPParse.report_frequency = 1000000
        class FakeDB(object):
            def __init__(self, dbname=None):
                self.records = {}
            def add_record(self, record, key):
                self.records[key] = record
        self.data = FakeDB()

    def tearDown(self):
        self.xml_file.close()

    def test_parse_one_record(self):
        self.DBLPParse.parse(self.data, self.xml_file, max_records=1)
        self.assertEqual(len(self.data.records), 1)

    def test_parse_all(self):
        self.DBLPParse.parse(self.data, self.xml_file)
        self.assertEqual(len(self.data.records), 6)


class Test_DBDBLP(unittest.TestCase):

    import DBDBLP

    def setUp(self):
        self.tmp_fname = '._tmp.db'
        self.new_key = '__new_key__'
        with open('miniDB.records') as f:
            content = f.read()
        self.records = eval(content)
        self.pubs = {}
        for k in self.records:
            self.pubs[k] = Publication(from_DBLP=self.records[k])
            self.pubs[k].key = k

    def key_lookup(self, db):
        for k in self.records:
            from_db = db.by_key(k)
            for f in from_db.fieldnames:
                self.assertEqual(from_db.__dict__[f], self.pubs[k].__dict__[f])

    def author_lookup(self, db):
        for k in self.records:
            authors = self.pubs[k].author
            for a in authors:
                from_db = db.by_author(a)
                for p in from_db:
                    self.assertTrue(Authors([a]) <= p.author)

    def title_lookup(self, db):
        for k in self.records:
            title = self.pubs[k].title
            from_db = db.by_title(title)
            for p in from_db:
                self.assertTrue(title <= p.title)

    def hint_lookup(self, db):
        for k in self.records:
            title = self.pubs[k].title
            authors = self.pubs[k].author
            from_db = db.by_hint(authors, title)
            for p in from_db:
                self.assertTrue(authors <= p.author and title <= p.title)
                
    def test_memory_lookup(self):
        db = self.DBDBLP.DBLPMemory()
        for k in self.records:
            db.add_record(self.records[k], k)
        db.done()
        self.key_lookup(db)
        self.author_lookup(db)
        self.title_lookup(db)
        self.hint_lookup(db)

    def same_elements(self, db, key1, key2):
        el1 = db.by_key(key1)
        el2 = db.by_key(key2)
        for k in el1.__dict__:
            if k != 'key':
                self.assertEqual(el1.__dict__[k], el2.__dict__[k])

    @unittest.skipIf(os.path.isfile('._tmp.db'),
                     'Temporary database file exists. Remove it to run this test.')
    def test_persistent_lookup(self):
        if not os.path.isfile(self.tmp_fname):
            db = self.DBDBLP.DBLPPersistent(dbname=self.tmp_fname)
            db.init_tables()
            for k in self.records:
                db.add_record(self.records[k], k)
            db.done()
            db = self.DBDBLP.DBLPPersistent(dbname=self.tmp_fname)
            self.key_lookup(db)
            self.author_lookup(db)
            self.title_lookup(db)
            self.hint_lookup(db)
            db = self.DBDBLP.DBLPUpdatable(dbname=self.tmp_fname)
            # This should not trigger any failure, since an element with that key already exists
            old_key = self.records.keys()[0]
            db.add_record(None, old_key)
            # This inserts a duplicate
            db.add_record(self.records[old_key], self.new_key)
            # Check that the duplicates are the same
            self.same_elements(db, old_key, self.new_key)
            os.remove(self.tmp_fname)


class Test_bibfromhints(unittest.TestCase):

    import bibfromhints

    class StubDB(object):
        def __init__(self, dbname=None):
            self.pub = Publication(from_dict={'btype': 'inproceedings',
                                              'key': 'key',
                                              'title': Title('title: long and vacuous'),
                                              'author': Authors([Author('author1'),
                                                                 Author('author2')]),
                                              'crossref': 'crossref',
                                              'year': '2020',
                                              'month': 'month',
                                              'pages': '0-1000'})
            self.cross = Publication(from_dict={'key': 'crossref',
                                                'title': Title('booktitle ICSE conference'),
                                                'editor': Authors([Author('editor')]),
                                                'month': 'month'})
        def by_key(self, key):
            return self.cross
        def by_title(self, title):
            return set([self.pub])
        def by_hint(self, authors, title):
            return set([self.pub])

    def setUp(self):
        self.acronyms = self.bibfromhints.conference_acronyms()
        self.db = self.StubDB()
        self.pub = self.db.pub
        self.cross = self.db.cross
        self.hints = [{'authors': self.pub.author, 'title': ""},
                      {'authors': Authors([]), 'title': ""}]

    def test_clean_record(self):
        p = self.bibfromhints.clean_record(self.pub, self.db, acronyms=self.acronyms)
        self.assertEqual(p.booktitle, self.cross.title)
        self.assertEqual(p.month, self.cross.month)
        self.assertEqual(p.editor, self.cross.editor)
        self.assertEqual(p.acronym, 'ICSE')
        self.assertTrue(type(p.pages) is tuple)
        self.assertEqual(p.pages[0], '0')
        self.assertEqual(p.pages[1], '1000')
        self.assertNotEqual(p.key, '')

    def test_pubs_from_hints(self):
        pubs = self.bibfromhints.pubs_from_hints(self.hints, self.db, clean=False)
        for p in pubs:
            self.assertEqual(p, self.pub)


class Test_rec2bib(unittest.TestCase):

    import rec2bib

    def setUp(self):
        self.pub = Publication(from_dict={'btype': 'article',
                                          'key': 'key',
                                          'title': Title('Use C# in title'),
                                          'author': Authors([Author(u'John P. Döe'),
                                                             Author('Jane {de Roe}')]),
                                          'booktitle': Title('a conference'),
                                          'acronym': 'ACR',
                                          'year': '2020',
                                          'month': 'month',
                                          'pages': '0-1000'})
        self.pub = self.rec2bib.FormattablePublication(from_other=self.pub)
        
    def test_only_attributes(self):
        only = ['btype', 'key', 'title', 'author', 'booktitle', 'acronym', 'year', 'pages']
        self.pub.only_attributes(only)
        for f in self.pub.fieldnames:
            if f not in only:
                self.assertEqual(self.pub.__dict__[f], "")
            else:
                self.assertEqual(self.pub.__dict__[f], self.pub.__dict__[f])

    def test_latex_escape(self):
        self.pub.latex_escape()
        try:
            import pylatexenc
            self.assertEqual(unicode(self.pub.author.authors[0]), u'John P. D{\\"o}e')
            self.assertEqual(unicode(self.pub.author.authors[1]), u'Jane {de Roe}')
        except ImportError:
            self.assertEqual(unicode(self.pub.author.authors), u'John P. De')

    def test_latex_brace(self):
        self.rec2bib.proper_names = self.rec2bib.get_proper_names()
        self.pub.latex_brace()
        self.assertEqual(unicode(self.pub.title), u'Use {C#} in title')

    def test_latex_ranges(self):
        self.pub.latex_ranges()
        self.assertEqual(self.pub.pages, u'0--1000')

    def test_acronym_only(self):
        fmstr = 'Proceedings of %s this year'
        self.pub.acronym_only(fmstr)
        self.assertEqual(unicode(self.pub.booktitle), fmstr % self.pub.acronym)

    def test_unique_key(self):
        k = self.pub.key
        self.pub.unique_key([k + 'no'])
        self.assertEqual(self.pub.key, k)
        self.pub.unique_key([k])
        self.assertNotEqual(self.pub.key, k)


if __name__ == '__main__':
    bibdata_1 = unittest.TestLoader().loadTestsFromTestCase(Test_bibdata_Authors_Title)
    bibdata_2 = unittest.TestLoader().loadTestsFromTestCase(Test_bibdata_Publication)
    dblp_parse = unittest.TestLoader().loadTestsFromTestCase(Test_DBLPParse)
    dbdblp = unittest.TestLoader().loadTestsFromTestCase(Test_DBDBLP)
    bibfromhints = unittest.TestLoader().loadTestsFromTestCase(Test_bibfromhints)
    rec2bib = unittest.TestLoader().loadTestsFromTestCase(Test_rec2bib)
    suite = unittest.TestSuite([bibdata_1, bibdata_2, dblp_parse, dbdblp,
                                bibfromhints, rec2bib])
    unittest.TextTestRunner(verbosity=2, buffer=True).run(suite)
