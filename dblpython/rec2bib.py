#! /usr/bin/env python

import argparse
import fileinput
import sys, os.path
import json
import re
from bibdata import Publication, BibJSON


PROPER_NAMES = 'proper_names.txt'


def get_proper_names():
    res = []
    import os.path
    try:
        _proper_names = os.path.join(os.path.dirname(os.path.abspath(__file__)), PROPER_NAMES)
        with open(_proper_names) as f:
            for line in f:
                res.append(line.strip())
    except:
        pass
    return res

proper_names = []


class FormattablePublication(Publication):
    """Data for one publication, with methods to display it in text and BibTeX formats."""

    try:
        from pylatexenc import latexencode
        def tex_encode(self, c):
            return self.latexencode.utf8tolatex(c)
    except ImportError:
        print >> sys.stderr, "Warning: I need module pylatexenc to escape characters in TeX. Using a crude ASCII encoding instead."
        def tex_encode(self, c):
            return c.encode('ascii', 'ignore')

    def app(self, function, in_attributes=Publication().fieldnames.keys()):
        """Apply function to all attributes in in_attributes."""
        for f in in_attributes:
            try:
                v = self.__dict__[f]
                if type(v) is str or type(v) is unicode:
                    v = function(v)
                elif hasattr(v, 'app'):
                    v.app(function)
                elif type(v) is list:
                    v = map(function, v)
                else:
                    assert False
                self.__dict__[f] = v
            except KeyError:
                continue

    def only_attributes(self, keep_attributes):
        """Set all attributes other than those in keep_attributes to the empty string."""
        for f in self.fieldnames:
            if f not in keep_attributes and f not in ['btype', 'key']:
                try:
                    self.__dict__[f] = ""
                except KeyError:
                    continue
    
    def latex_escape(self):
        """Escape unicode characters into LaTeX sequences.""" 
        def utf8tolatex_butbraces(s):
            """Apply utf8tolatex to all characters but braces."""
            return "".join([self.tex_encode(c) if c not in ['{', '}'] else c for c in s])
        self.app(utf8tolatex_butbraces)

    def latex_brace(self):
        """Add braces around likely acronyms in all given fields."""
        def is_acronym(s):
            if s in proper_names:
                print "AA", s
            return s in proper_names or re.match('[A-Z]\w*[A-Z]+\w*', s)
        def brace(s):
            splitted = re.split('(\s)+', s)
            return "".join(['{' + x + '}' if is_acronym(x) else x for x in splitted])
        self.app(brace)

    def latex_ranges(self):
        """Rewrite ranges using an N-dash."""
        def n_dash(s):
            m = re.match('^(\d+)(-+)(\d+)$', s)
            if m:
                return m.group(1) + '--' + m.group(3)
            return re.sub('(\s)(\d+)(-+)(\d+)', r'\1\2--\4', s)
        self.app(n_dash, in_attributes=['booktitle', 'pages'])

    def acronym_only(self, fstr):
        """Replace booktitle with:
           fstr % self.acronym
        if an acronym exists and is non-empty."""
        try:
            acronym = self.acronym
            if len(acronym) > 0:
                self.booktitle = fstr % acronym
        except AttributeError:
            pass

    def unique_key(self, other_keys=[]):
        """Change self.key (if necessary) so that it is different from all in other_keys."""
        basekey = self.key if self.key != "" else "bib"
        key = basekey
        n = 0
        while key in other_keys:
            n += 1
            key = basekey + ('-%d' % n)
        self.key = key

    def bibtex(self):
        """Publication data in BibTeX format.
        Only non-empty fields are included."""
        btype = self.btype
        key = self.key
        maxlen = max(map(len, self.fieldnames))
        entries = []
        for f in self.fieldnames:
            if f in ['btype', 'key']:
                continue
            elif f in ['author', 'editor']:
                v = " and ".join([unicode(a) for a in self.__dict__[f]])
            else:
                v = unicode(self.__dict__[f])
            if v != "":
                entries.append('%-*s = {%s}' % (maxlen, f, v))
        return '@%s{%s,\n' % (btype, key) + ',\n'.join(entries) + '\n}'

    def text(self, style='plain'):
        """Publication data in plain text format.
        Only non-empty fields are included."""
        try:
            from pybtex import PybtexEngine
            from pybtex.exceptions import PybtexError
            q = FormattablePublication(from_other=self)
            q.crossref = ""
            try:
                res = PybtexEngine().format_from_string(q.bibtex(), style, output_backend='plaintext')
            except PybtexError as ex:
                print >> sys.stderr, "Error in pybtex:\n%s\nGenerating DSV format instead." % ex.message
                res = unicode(self)
            if res.startswith('[1] '):
                res = res[4:]
            return res
        except ImportError:
            print >> sys.stderr, "Warning: I need module pybtex to format in text format. Using a DSV format instead."
            return unicode(self)

                
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Formats bibliographi information available in JSON format.')
    parser.add_argument('-o', '--output', dest='output', choices=['text', 'bib'], default='bib',
                        help='output format (default: %(default)s).')
    parser.add_argument('-F', '--fields', dest='field', nargs='*',
                        help='which field names are printed (default: print all non-empty fields).')
    parser.add_argument('--conference', dest='conference_format',
                        help='format string with a single %%s, which will be replaced by the conference acronym giving the booktitle for inproceedings (include braces if necessary).')
    parser.add_argument('--style', dest='style', default='plain', action='store',
                        help='use this BibTeX style to format text output (default: %(default)s).')
    parser.add_argument('filename', nargs='?', 
                        help='file with JSON records to be processed (- or omit for standard input).')
    args = parser.parse_args()
    proper_names = get_proper_names()
    if args.filename == '-' or args.filename is None:
        f = sys.stdin.readlines()
    else:
        f = open(args.filename)
    keys = []
    for line in f:
        p = FormattablePublication(from_other=BibJSON.loads(line.strip()))
        if args.field is not None:
            p.only_attributes(args.field)
        if args.conference_format is not None:
            p.acronym_only(args.conference_format)
        if args.output == 'bib':
            p.latex_escape()
            p.latex_ranges()
            p.latex_brace()
            p.unique_key(other_keys=keys)
            keys.append(p.key)
            print p.bibtex()
        elif args.output == 'text':
            print p.text(style=args.style)
