import json

def simple_name(name, also_keep=[]):
    """Simplified version (keep only alphanumeric and characters in also_keep) of string name."""
    return "".join([c for c in name if c.isalnum() or c in also_keep]).lower()


class Author(object):
    """Single author name, compared according to simple_name."""

    def __init__(self, name):
        self.name = name
        self.simple_name = simple_name(self.name, also_keep=[' '])

    def __unicode__(self):
        return self.name

    def __str__(self):
        return unicode(self).encode('utf-8')
        
    def __hash__(self):
        return hash(self.simple_name)

    def __eq__(self, other):
        return self.__hash__() == other.__hash__()

    def app(self, function):
        """Apply text-modification function to author's name."""
        self.__init__(function(self.name))


class Authors(object):
    """List of authors, comparable by subset relation."""

    def __init__(self, authors=None):
        if authors is None:
            self.authors = list()
        else:
            self.authors = authors

    def add_author(self, author):
        self.authors.append(author)

    def as_set(self):
        return frozenset(self.authors)

    def __iter__(self):
        self.index = -1
        return self

    def next(self):
        self.index += 1
        if self.index >= len(self.authors):
            raise StopIteration
        else:
            return self.authors[self.index]

    def __unicode__(self):
        return ", ".join(map(unicode, self.authors))

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __hash__(self):
        return hash(self.as_set())

    def __len__(self):
        return len(self.authors)
    
    def __eq__(self, other):
        return self.__hash__() == other.__hash__()

    def __le__(self, other):
        return self.as_set() <= other.as_set()

    def __ge__(self, other):
        return other.__le__(self)

    def app(self, function):
        """Apply text-modification function to authors' names."""
        map(lambda a: a.app(function), self.authors)


class Title(object):
    """Single title, compared according to simple_name."""

    def __init__(self, title):
        self.title = title
        self.simple_title = simple_name(self.title, also_keep=[])

    def __unicode__(self):
        return self.title

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __hash__(self):
        return hash(self.simple_title)

    def __eq__(self, other):
        return self.__hash__() == other.__hash__()

    def __le__(self, other):
        return other.simple_title.startswith(self.simple_title)

    def __ge__(self, other):
        return other.__le__(self)

    def app(self, function):
        """Apply text-modification function to title."""
        self.__init__(function(self.title))


class Publication(object):
    """Data for one publication.

    Attributes:
       btype (BibTeX kind such as inproceedings, article, etc.)
       key (string key for reference)
       title (object of class Title)
       author (list of authors, object of class Authors)
       booktitle (name of book where the article appears)
       editor (list of editors, object of class Authors)
       journal (name of journal where the article appears)
       pages (page range)
       year
       month
       series (series of books where the article appears)
       volume
       number
       publisher
       ee (URI of electronic edition)
       acronym (acronym of conference where the article appears)
    """

    _fieldnames = {'btype': "",
                   'key': "",
                   'title': Title(""),
                   'author': Authors([]),
                   'booktitle': "",
                   'editor': Authors([]),
                   'journal': "",
                   'pages': "",
                   'year': "",
                   'month': "",
                   'series': "",
                   'volume': "",
                   'number': "",
                   'publisher': "",
                   'ee': "",
                   'school': "",
                   'institution': "",
                   'chapter': "",
                   'note': "",
                   'crossref': "",
                   'acronym': ""}

    @property
    def fieldnames(self):
        return type(self)._fieldnames

    @fieldnames.setter
    def fieldnames(self, fnms):
        self._fieldnames = fnms

    def __init__(self,
                 from_DBLP=None,
                 from_dict=None,
                 from_other=None):
        from bibfromhints import ACRONYMS_FILE, conference_acronyms
        """Construct object with attributes:

           no optional argument: set to defaults (as per fieldnames)
           from_DBLP: from DBLP record (a dictionary object)
           from_dict: from dictionary with keys equal to attribute names
           from_other: shallow copy from other Publication object
        """
        self.acronyms = conference_acronyms()
        if from_other is not None:
            self.fieldnames = from_other.fieldnames
            from_dict = {}
            for f in self.fieldnames:
                from_dict[f] = from_other.__dict__[f]
        elif from_dict is None:
            from_dict = self.fieldnames
        for f in self.fieldnames:
            try:
                v = from_dict[f]
            except KeyError:
                v = self.fieldnames[f]
            self.__dict__[f] = v
        if from_DBLP is not None:
            self.from_DBLP(from_DBLP)
        if type(self.pages) is list or type(self.pages) is tuple:
            self.pages = unicode(self.pages[0]) + '-' + unicode(self.pages[1])

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        maxlen = max(map(len, self.fieldnames))
        sorted_keys = ['btype', 'key'] + \
                      sorted([f for f in self.fieldnames if f not in ['btype', 'key']])
        return "\n".join(['%-*s = %s' % (maxlen, f, unicode(self.__dict__[f])) \
                          for f in sorted_keys])

    def __normalize(self):
        if not isinstance(self.title, Title):
            self.title = Title(self.title)
        if type(self.author) is str or type(self.author) is unicode:
            self.author = [self.author]
        if isinstance(self.author, list):
            self.author = Authors(map(lambda a: Author(a), self.author))
        if type(self.editor) is str or type(self.editor) is unicode:
            self.editor = [self.editor]
        if isinstance(self.editor, list):
            self.editor = Authors(map(lambda a: Author(a), self.editor))

    def guess_acronym(self):
        """Guess conference acronym."""
        import re
        if self.btype == "inproceedings":
            pieces = map(lambda s: s.strip(),
                         re.split("([-\s',;(){}])+", self.booktitle))
            for p in pieces:
                if p in self.acronyms:
                    self.acronym = p
                    break

    def get_venue(self):
        self.guess_acronym()
        if self.acronym != "":
            return self.acronym
        elif self.btype == "inproceedings":
            return self.booktitle
        else:
            return self.journal

    def from_DBLP(self, publication):
        """Populate self with data from DBLP record publication."""
        for f in self.fieldnames:
            f_prime = 'type' if f == "btype" else f
            try:
                d = publication[f_prime]
                if d is not None and d != [] and d != "":
                    self.__dict__[f] = d
            except KeyError:
                pass
        self.__normalize()


class BibJSON(json.JSONEncoder):

    def default(self, o):
        if isinstance(o, Author):
            return o.name
        elif isinstance(o, Authors):
            return o.authors
        elif isinstance(o, Title):
            return o.title
        elif isinstance(o, Publication):
            pub_dict = {}
            for f in o.fieldnames:
                try:
                    pub_dict[f] = o.__dict__[f]
                except AttributeError:
                    continue
            return pub_dict
        else:
            return json.JSONEncoder.default(self, o)

    @staticmethod
    def from_json(o):
        pub_dict = o
        if type(pub_dict) is dict:
            pub_dict['author'] = Authors(map(lambda a: Author(a), pub_dict['author']))
            pub_dict['editor'] = Authors(map(lambda e: Author(e), pub_dict['editor']))
            pub_dict['title'] = Title(pub_dict['title'])
            return Publication(from_dict=pub_dict)
        else:
            return pub_dict

    @staticmethod
    def loads(s):
        """Like json.loads, but specialized for Publication-related classes."""
        return json.loads(s, object_hook=BibJSON.from_json)

    @staticmethod
    def dumps(o, sort_keys=True):
        """Like json.dumps, but specialized for Publication-related classes."""
        return json.dumps(o, cls=BibJSON, sort_keys=sort_keys)
