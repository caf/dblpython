import sys, xml.sax


report_frequency = 100000


class MaxRecordsReachedException(xml.sax.SAXException):
    pass


class DBLPHandler(xml.sax.ContentHandler):

    _pub_types = set(['article', 'book', 'booklet', 'inproceedings', 'manual', 'incollection', 'mastersthesis', 'misc', 'phdthesis', 'proceedings', 'techreport',  'www'])

    @property
    def pub_types(self):
        return self._pub_types

    def __init__(self, data, max_records=0):
        self.data = data
        self.max_records = max_records
        self.in_record = False
        self.text = ''
        self.papercount = 0

    def startElement(self, name, attrs):
        if name in self.pub_types:
            self.in_record = True
            self.cur_record = {}
            self.cur_record['type'] = name
            self.cur_key = str(attrs['key'])
        else:
            self.text = ''

    def endElement(self, name):
        if self.in_record:
            if name in self.pub_types:
                self.papercount += 1
                try:
                    title = self.cur_record['title']
                except KeyError:
                    title = '_orphaned'
                self.data.add_record(self.cur_record, self.cur_key)
                self.in_record = False
                self.check_progress()
            elif name in self.cur_record.keys():
                if type(self.cur_record[name]) is list:
                    self.cur_record[name].append(self.text.strip())
                else:
                    self.cur_record[name] = [self.cur_record[name], self.text.strip()]
            else:
                self.cur_record[name] = self.text.strip()

    def characters(self, chars):
        self.text += chars

    def check_progress(self):
        if self.papercount % report_frequency == 0:
            print >> sys.stderr, '... processed %d papers so far ...' % self.papercount
        if self.papercount >= self.max_records and self.max_records > 0:
            raise MaxRecordsReachedException("Reached max record limit of %d records processed." % self.max_records)


def parse(data_object, xml_file, max_records=0):
    """Parse DBLP database and return DBLPDB object.
    Parse only the first max_records if max_records > 0."""
    dblp = DBLPHandler(data_object, max_records=max_records)
    try:
        parser = xml.sax.parse(xml_file, dblp)
    except MaxRecordsReachedException as e:
        print >> sys.stderr, e.message
    finally:
        print >> sys.stderr, '** Done reading DBLP database: %d records read.' % dblp.papercount
    return data_object
