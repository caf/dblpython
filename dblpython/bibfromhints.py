#! /usr/bin/env python

import argparse
import os, sys, gzip
from bibdata import simple_name, BibJSON
from bibdata import Author, Authors, Title, Publication
import DBDBLP, DBLPParse


ACRONYMS_FILE = "CS_conferences.txt"
DB_FILE = "DBLP.db"


DBLP_url = 'http://dblp.uni-trier.de/xml/dblp.xml.gz'
DTD_url = 'http://dblp.uni-trier.de/xml/dblp.dtd'
dblp_filename = 'dblp.xml.gz'
dtd_filename = 'dblp.dtd'


def conference_acronyms():
    """Set of recognized conference acronyms."""
    res = []
    import os.path
    _acronyms_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), ACRONYMS_FILE)
    with open(_acronyms_file) as f:
        for line in f:
            res.append(line.strip())
    return set(res)


def clean_record(publication, db, acronyms=set([]), key_len=12):
    """Clean publication record (object of class Publication).

       1. Extract booktitle, editor, publisher, series, and month data from crossref
       2. Remove spaces between slashes in publisher name
       3. Strip numeric ids in names (used in DBLP to distinguish between people with same name)
       4. Convert page ranges into pairs of integers
       5. Try to guess conference acronym (must be in acronyms)
       6. Generate key of up to key_len characters
    """
    import re
    # for papers in proceedings, we collect additional data from cross reference to the proceedings
    if publication.btype == "inproceedings":
        try:
            crossref = db.by_key(publication.crossref)
            publication.booktitle = crossref.title
            publication.editor = crossref.editor
            publication.publisher = crossref.publisher
            publication.series = crossref.series
            publication.month = crossref.month if crossref.month != "" else publication.month
        except (AttributeError, KeyError) as x:
            print x.message
            print >> sys.stderr, "Failed to update crossref of %s" % publication.key
    # Publisher: remove spaces between slashes
    publication.publisher = re.sub('\s+/\s+', lambda s: '/', publication.publisher)
    # Authors and Editors: strip number ids in names
    publication.author = Authors([Author(re.sub('\s+\d*$', '', unicode(a))) for a in publication.author])
    publication.editor = Authors([Author(re.sub('\s+\d*$', '', unicode(a))) for a in publication.editor])
    # Pages: turn page ranges into pairs
    pgs = re.split('-+', publication.pages)
    if len(pgs) == 2:
        publication.pages = (pgs[0], pgs[1])
    # Guess conference acronym
    if publication.btype == "inproceedings":
        pieces = map(lambda s: s.strip(), re.split("([-\s',;(){}])+", publication.booktitle.title))
        for p in pieces:
            if p in acronyms:
                publication.acronym = p
                break
    # Generate key
    if not hasattr(publication, 'genkey'):
        twords = [w[:4] for w in re.split('\s+', simple_name(publication.title.title, also_keep=[" "]))]
        publication.genkey = "".join(map(lambda x: x.capitalize(), twords[:(key_len // 4)])) + (unicode(publication.year)[-2:] if publication.year is not None else "")
    return publication


def read_hint(line):
    """Read "author:title" hint into Authors, Title objects."""
    import re
    try:
        author, title = re.split(':', line.strip(), maxsplit=1)
    except:
        author = ""
        title = ""
    try:
        authors = re.split('\s*,\s*', author.strip())
    except:
        authors = []
    authors = [a for a in authors if a != ""]
    return {'authors': Authors(map(lambda a: Author(a), authors)), 'title': Title(title)}


def read_hints(filename):
    """Read "author:title" hints from filename, one per line."""
    hints = []
    with open(filename) as f:
        for line in f:
            hints.append(read_hint(line))
    return hints


def update_file(url, out, update=False):
    """Return True iff out file has been updated."""
    from urllib import urlopen
    import datetime, os, os.path
    if os.path.isfile(out) and update:
        remote = datetime.date(*urlopen(url).info().getdate('last-modified')[:3])
        local = datetime.date.fromtimestamp(os.path.getmtime(out))
        if remote <= local:
            return False
        else:
            os.remove(out)
    if not os.path.isfile(out):
        try:
            import wget
            wget.download(url, out=out)
            return True
        except ImportError:
            print >> sys.stderr, 'Error: I need the module wget to download\n   %s\nInstall the module or download the file into %s' % (url, out)
            sys.exit(1)
    else:
        return False


def db_object(dbname, path, update=False):
    """Return a DBDBLP object linked to name dbname in path.
    Use :memory" for transient in-memory database."""
    _dblp_filename = os.path.join(path, dblp_filename)
    updated = update_file(DBLP_url, _dblp_filename, update=update)
    _dtd_filename = os.path.join(path, dtd_filename)
    updated = updated or update_file(DTD_url, _dtd_filename, update=update)
    xml_file = gzip.GzipFile(_dblp_filename, 'r')
    if dbname == ":memory:":
        print >> sys.stderr, "Building transient in-memory database. Please be patient!"
        db = DBDBLP.DBLPMemory()
        DBLPParse.parse(db, xml_file)
    else:
        dbname = os.path.join(path, dbname)
        if not os.path.exists(dbname):
            print >> sys.stderr, "\nCreating database file. Please be patient!"
            db = DBDBLP.DBLPPersistent(dbname)
            db.init_tables()
            DBLPParse.parse(db, xml_file)
            db.done()
        elif updated:
            print >> sys.stderr, "\nUpdating database file. This will take a few minutes."
            db = DBDBLP.DBLPUpdatable(dbname)
            DBLPParse.parse(db, xml_file)
            db.done()
        db = DBDBLP.DBLPPersistent(dbname)
    return db


def pubs_from_hints(hints, db, acronyms=[], clean=True):
    publications = set([])
    for h in hints:
        authors = h['authors']
        title = h['title']
        if len(authors.authors) > 0:
            publications |= db.by_hint(authors, title)
        else:
            publications |= db.by_title(title)
    if clean:
        publications = set([clean_record(p, db, acronyms=acronyms) for p in publications])
    return publications
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Lookup complete bibliographic records from DBLP from partial information.')
    parser.add_argument('-d', '--database', dest='dbname', default=DB_FILE, action='store',
                        help='database file name in script directory (default: %(default)s); use :memory: for transient in-memory database')
    parser.add_argument('-a', '--author', dest='author', nargs='+', 
                        help='list of author names')
    parser.add_argument('-t', '--title', dest='title', action='store', 
                        help='title (or beginning of title)')
    parser.add_argument('-u', '--update', dest='update', action='store_true', 
                        help='update local database with latest DBLP version')
    parser.add_argument('filename', nargs='?', 
                        help='file with partial information (line format: author1, author2, [...] : beginning of title)')
    args = parser.parse_args()
    script_dir = os.path.dirname(os.path.abspath(__file__))
    if args.filename is not None:
        hints = read_hints(args.filename)
    elif args.title is not None:
        author_list = args.author if args.author is not None else []
        hints = [read_hint(", ".join(author_list) + " : " + args.title)]
    else:
        print >> sys.stderr, "Either a title or a file with author/title information is required."
        sys.exit(1)
    acronyms = conference_acronyms()
    db = db_object(args.dbname, script_dir, update=args.update)
    print >> sys.stderr, "Database ready for querying."
    publications = pubs_from_hints(hints, db, acronyms=acronyms, clean=True)
    for p in publications:
        print BibJSON.dumps(p)
