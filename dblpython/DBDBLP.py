import sys
import sqlite3
from bibdata import Publication, BibJSON


class DBLPDB(object):
    """DBLP database interface."""

    def __init__(self, dbname=None):
        """dbname is a filename where to store the database."""
        pass

    def add_record(self, record, key):
        """Add DBLP record with key to database."""
        pass

    def done(self):
        """All records added: commit."""
        pass

    def by_key(self, key):
        """Publication object with exect key.
           Throws a KeyError exception if no record with key exists."""
        raise KeyError("Record with key %s not found." % key)

    def by_author(self, author):
        """Set of publications objects such that author (class Author) is == one of the authors."""
        return set([])

    def by_title(self, title):
        """Set of publication objects such that title (class Title) is <= their titles."""
        return set([])

    def by_hint(self, authors, title):
        """Set of publication objects such that authors (class Authors) is <= their authors and title is <= their titles."""
        return set([])

    def active_in_venues(self, venues, up_to = None, span = 10):
        """Set of authors who published in venues (conference acronyms or journal names) in years up_to - span + 1..up_to. If up_to is None, it is interpreted as the current year."""
        return (set([]))


class DBLPMemory(DBLPDB):
    """DBLP database in memory, indexed by key and by (simplified) author name."""

    def __init__(self, dbname=None):
        self._by_key = {}
        self._by_author = {}

    def add_record(self, record, key):
        record['key'] = key
        pub = Publication(from_DBLP=record)
        self._by_key[key] = pub
        for author in pub.author:
            try:
                self._by_author[author.simple_name].append(pub)
            except KeyError:
                self._by_author[author.simple_name] = [pub]

    def by_key(self, key):
        try:
            return self._by_key[key]
        except KeyError:
            super(DBLPMemory, self).by_key(key)

    def by_author(self, author):
        try:
            return set(self._by_author[author.simple_name])
        except KeyError:
            super(DBLPMemory, self).by_author(key)

    def by_title(self, title):
        """Sequential search (slow)."""
        res = []
        for k in self._by_key:
            d = self._by_key[k]
            if title <= d.title:
                res.append(d)
        return set(res)

    def by_hint(self, authors, title):
        res = []
        for a in authors:
            pubs = self.by_author(a)
            for p in pubs:
                if authors <= p.author and title <= p.title:
                    res.append(p)
        return set(res)


class DBLPPersistent(DBLPDB):
    """DBLP database in SQLite DB."""

    def __init__(self, dbname="DBLP.db"):
        self.dbname = dbname
        self.connection = sqlite3.connect(self.dbname)
        self.cursor = self.connection.cursor()

    def init_tables(self):
        self.cursor.execute("""CREATE TABLE publications
                               (key TEXT, js JSON, yearpub INTEGER,
                               PRIMARY KEY (key))""")
        self.cursor.execute("""CREATE TABLE authors
                               (key TEXT, nauthor INTEGER, author TEXT,
                               PRIMARY KEY (key, nauthor),
                               FOREIGN KEY (key) REFERENCES publications(key))""")
        self.cursor.execute("""CREATE TABLE titles
                               (key TEXT, title TEXT,
                               PRIMARY KEY (key),
                               FOREIGN KEY (key) REFERENCES publications(key))""")
        self.cursor.execute("""CREATE TABLE venues
                               (key TEXT, venue TEXT,
                               PRIMARY KEY (key),
                               FOREIGN KEY (key) REFERENCES publications(key))""")

    def add_record(self, record, key):
        record['key'] = key
        pub = Publication(from_DBLP=record)
        try:
            pubyear = int(pub.year)
        except ValueError:
            pubyear = 0
        self.cursor.execute("""INSERT INTO publications VALUES
                               (?, ?, ?)""", (pub.key, BibJSON.dumps(pub), pubyear))
        self.cursor.execute("""INSERT INTO titles VALUES
                               (?, ?)""", (pub.key, pub.title.simple_title))
        venue = pub.get_venue()
        self.cursor.execute("""INSERT INTO venues VALUES
                               (?, ?)""", (pub.key, venue))
        n = 0
        for author in pub.author:
            n += 1
            self.cursor.execute("""INSERT INTO authors VALUES
                                   (?, ?, ?)""", (pub.key, n, author.simple_name))

    def done(self):
        self.connection.commit()
        self.connection.close()

    def by_key(self, key):
        res = []
        for row in self.cursor.execute("""SELECT DISTINCT js 
                                          FROM publications 
                                          WHERE key = ?""", (key,)):
            p = BibJSON.loads(row[0])
            res.append(p)
        if len(res) == 0:
            super(DBLPPersistent, self).by_key(key)
        elif len(res) > 1:
            print >> sys.stderr, "Two entries with key %s, returning the first one (but check integrity constraints)." % key
        return res[0]

    def by_author(self, author):
        res = []
        for row in self.cursor.execute("""SELECT DISTINCT publications.js
                                          FROM publications NATURAL JOIN authors 
                                          WHERE authors.author = ?""", (author.simple_name,)):
            p = BibJSON.loads(row[0])
            res.append(p)
        return set(res)

    def by_title(self, title):
        res = []
        for row in self.cursor.execute("""SELECT DISTINCT publications.js
                                          FROM publications NATURAL JOIN titles 
                                          WHERE titles.title LIKE ?""", (title.simple_title + "%",)):
            p = BibJSON.loads(row[0])
            res.append(p)
        return set(res)

    def by_hint(self, authors, title):
        res = []
        # populate temp table with all authors in hint
        self.cursor.execute("""CREATE TEMPORARY TABLE hintAuthors (hauthor TEXT)""")
        nauthors = 0
        for author in authors:
            nauthors += 1
            self.cursor.execute("""INSERT INTO hintAuthors VALUES (?)""", (author.simple_name,))
        for row in self.cursor.execute(
                """
                SELECT js
                FROM
                  titles
                    NATURAL JOIN
                  (SELECT key, js
                    FROM
                      publications
                         NATURAL JOIN
                      (SELECT DISTINCT key
                       FROM authors JOIN hintAuthors ON author = hauthor
                       GROUP BY key HAVING COUNT(key) >= ?))
                WHERE titles.title LIKE ?
                """, (nauthors, title.simple_title + "%")):
            p = BibJSON.loads(row[0])
            res.append(p)
        self.cursor.execute("""DROP TABLE hintAuthors""")
        return set(res)

    def active_in_venues(self, venues, up_to = None, span = 10):
        if up_to is None:
            import datetime
            up_to = datetime.datetime.now().year
        back_from = up_to - span + 1
        res = []
        for row in self.cursor.execute("""SELECT DISTINCT authors.author
                                          FROM publications NATURAL JOIN authors NATURAL JOIN venues
                                          WHERE (publications.yearpub BETWEEN ? AND ?)
                                          AND (venues.venue IN (%s))""" % ",".join("?"*len(venues)),
                                       (back_from, up_to) + tuple(venues)):
            res.append(row)
        return set(res)


class DBLPUpdatable(DBLPPersistent):
    """Like DBLPPersistent, where add_record only adds new records."""

    def add_record(self, record, key):
        """Add record with key, unless a record with key already exists."""
        try:
            self.by_key(key)
        except KeyError:
            # if a record with key is not already present, insert it
            super(DBLPUpdatable, self).add_record(record, key)
